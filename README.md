# Deployment

## Using Heroku CLI

- Register di heroku
- Buat aplikasi baru di heroku
- Tambahkan react buildpack ke dalam aplikasi heroku di setting

```
https://github.com/mars/create-react-app-buildpack.git
```

- Buat react app baru
- Login dengan menggunakan heroku CLI dalam terminal

```bash
$ heroku login
```

- Buat git remote ke dalam aplikasi heroku yang sudah kita buat

```bash
$ heroku git:remote -a <your-heroku-app>
```

- Push ke heroku

```bash
$ git add .
$ git commit -m 'Create heroku using CLI'
$ git push origin master
```

- Buka hasil

```bash
$ heroku open
```
